package Model;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Examen", uniqueConstraints = {
        @UniqueConstraint(columnNames = "idExamen"),
        @UniqueConstraint(columnNames = "nombreExamen")

})
public class Examen implements Serializable {

    @Id
    @Column(name = "idExamen")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @NaturalId
    @Column(name = "nombreExamen")
    private String nombre;

    @Column(name = "activo")
    private Integer activo = 1;

    @ManyToOne(cascade = CascadeType.ALL)
 /*   @JoinColumn(name = "idDocente", nullable = false)
 */   private Docente docente;


    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "examen")
/*
    @JoinColumn(name = "Examen_idExamen")
*/
    private List<Pregunta> preguntaList =new ArrayList<Pregunta>();


    public Examen(String nombre, Docente docente) {
        this.nombre = nombre;
        this.docente = docente;
    }
    public  Examen(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public List<Pregunta> getPreguntaList() {
        return preguntaList;
    }

    public void setPreguntaList(List<Pregunta> preguntaList) {
        this.preguntaList = preguntaList;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

}
