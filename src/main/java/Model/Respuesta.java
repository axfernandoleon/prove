package Model;

import javax.persistence.*;
import java.io.Serializable;

public class Respuesta implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(name = "idRespuesta")
    private Long id;

    @Column(name = "nombreRespuesta")
    private   String nombre;


    @ManyToOne(cascade = CascadeType.ALL)
    private Pregunta pregunta;


    public Respuesta() {
    }


    public Respuesta(String nombre, Pregunta pregunta) {
        this.nombre = nombre;
        this.pregunta = pregunta;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    @Override
    public String toString() {
        return "Respuesta{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", pregunta=" + pregunta +
                '}';
    }
}
