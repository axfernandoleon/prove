package Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Resultado",uniqueConstraints = {
        @UniqueConstraint(columnNames = "idResultado")
})
public class Resultado implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(name = "idResultado")
    private Integer id;


    /*@ManyToOne(cascade = CascadeType.ALL)
    private Estudiante estudiante;
    */

   /* @ManyToOne(cascade = CascadeType.ALL)
    private Examen examen;

    @ManyToOne(cascade = CascadeType.ALL)
    private Pregunta pregunta;
*/
    @Column(name = "Examen_idExamen")
    private Integer id_examen;

    @Column(name = "Pregunta_idPregunta")
    private Integer id_pregunta;

    @Column(name = "nombre_estudiante")
    private  String nombre_estudiante;

    @Column(name = "respuestaSelect")
    private String respuestaSelect;

    public  Resultado(){}


    public Resultado(Integer id_examen, Integer id_pregunta, String nombre_estudiante, String respuestaSelect) {
        this.id_examen = id_examen;
        this.id_pregunta = id_pregunta;
        this.nombre_estudiante = nombre_estudiante;
        this.respuestaSelect = respuestaSelect;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_examen() {
        return id_examen;
    }

    public void setId_examen(Integer id_examen) {
        this.id_examen = id_examen;
    }

    public Integer getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(Integer id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

    public String getNombre_estudiante() {
        return nombre_estudiante;
    }

    public void setNombre_estudiante(String nombre_estudiante) {
        this.nombre_estudiante = nombre_estudiante;
    }

    public String getRespuestaSelect() {
        return respuestaSelect;
    }

    public void setRespuestaSelect(String respuestaSelect) {
        this.respuestaSelect = respuestaSelect;
    }
}
