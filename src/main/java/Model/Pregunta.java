package Model;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Pregunta",uniqueConstraints = {
        @UniqueConstraint(columnNames = "idPregunta"),
        @UniqueConstraint(columnNames = "nombrePregunta")
})
public class Pregunta {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(name = "idPregunta")
    private Integer id;
    @NaturalId
    @Column(name = "nombrePregunta")
    private String nombre;

    @ManyToOne(cascade = CascadeType.ALL)
    private Examen examen;

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "pregunta" )
    private List<Alternativa> alternativas = new ArrayList<Alternativa>();


    @Column(name = "Categoria_idCategoria")
    private  Integer categoria;

    public Pregunta(String nombrePregunta, Examen examen) {
        this.nombre = nombrePregunta;
        this.examen = examen;
    }

    public Pregunta(String nombre, Examen examen, List<Alternativa> alternativas, Integer categoria) {
        this.nombre = nombre;
        this.examen = examen;
        this.alternativas = alternativas;
        this.categoria = categoria;
    }

    public Pregunta(String nombre, Examen examen, Integer categoria) {
        this.nombre = nombre;
        this.examen = examen;
        this.categoria = categoria;
    }

    public  Pregunta(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombrePregunta() {
        return nombre;
    }

    public void setNombrePregunta(String nombrePregunta) {
        this.nombre= nombrePregunta;
    }

    public Examen getExamen() {
        return examen;
    }

    public void setExamen(Examen examen) {
        this.examen = examen;
    }

    public List<Alternativa> getAlternativas() {
        return alternativas;
    }

    public void setAlternativas(List<Alternativa> alternativas) {
        this.alternativas = alternativas;
    }

    @Override
    public String toString() {
        return "Pregunta{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", examen=" + examen +
                ", alternativas=" + alternativas +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }
}
