package Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Alternativa")
public class Alternativa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "idAlternativa")
    private Integer id;

    @Column(name = "nombreAlternativa")
    private String nombre;


    @ManyToOne(cascade = CascadeType.ALL)
    private Pregunta pregunta;


    public Alternativa() {
    }


    public Alternativa(String nombre, Pregunta pregunta) {
        this.nombre = nombre;
        this.pregunta = pregunta;
    }

    public Alternativa(String nombre) {
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    @Override
    public String toString() {
        return "Alternativa{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", pregunta=" + pregunta +
                '}';
    }
}
