package Model;

import javax.persistence.*;
import java .io.Serializable;
import java.util.*;

@Entity
@Table(name = "Docente",uniqueConstraints = {
        @UniqueConstraint(columnNames = "idDocente")
})
public class Docente implements Serializable {

    private  static  final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=  GenerationType.SEQUENCE)
    @Column(name = "idDocente")
    private Integer id;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "pass")
    private String pass;

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "docente" )
  /*  @JoinColumn(name = "Docente_idDocente")
 */
    private List<Examen> examenList = new ArrayList<Examen>();

    public Docente(String usuario, String pass) {
        this.usuario = usuario;
        this.pass = pass;
    }

    public  Docente(){

    }

    public Docente(String usuario) {
        this.usuario = usuario;
    }



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public List<Examen> getExamenList() {
        return examenList;
    }

    public void setExamenList(List<Examen> examenList) {
        this.examenList = examenList;
    }


    @Override
    public String toString() {
        return "Docente{" +
                "id=" + id +
                ", usuario='" + usuario + '\'' +
                ", pass='" + pass + '\'' +
                ", examenList=" + examenList +
                '}';
    }
}
