package WS;

import Controller.Controller;
import Dao.ExamenDaoImp;
import Model.Alternativa;
import Model.Examen;
import Model.Pregunta;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.api.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

@WebSocket
public class TestSocket {
    private static final Queue<Session> sessions = new ConcurrentLinkedQueue<>();
    private Integer id_examen =0;


    @OnWebSocketConnect
    public void connected(Session session) {
        sessions.add(session);
        System.out.println("Connected");
    }

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) throws  IOException{
        sessions.remove(session);
        System.out.println("Connected");
    }
    @OnWebSocketMessage
    public void onMessage(Session session, String message) throws  IOException{

        System.out.println(message);
        setId_examen(Integer.valueOf(message));
        try {
            if (session.isOpen()) {
                session.getRemote().sendString(getTest(id_examen));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Integer getId_examen() {
        return id_examen;
    }

    public void setId_examen(Integer id_examen) {
        this.id_examen = id_examen;
    }


    private static String getTest(Integer id) {
        ExamenDaoImp examenDaoImp = new ExamenDaoImp();
        Examen examen = examenDaoImp.findByIdExamen(id);
        JSONObject responseDetail = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        List<Pregunta> preguntas = examen.getPreguntaList();

        Set<Pregunta> linkhashSet = new HashSet<Pregunta>(preguntas);
        linkhashSet.addAll(preguntas);
        preguntas.clear();
        preguntas.clear();
        preguntas.addAll(linkhashSet);
        JSONObject blank = new JSONObject();
        blank.put("id",null);
        blank.put("pregunta",null);
        blank.put("tipo_pregunta",1);
        System.out.println(preguntas.size());
        for (Pregunta pregunta : preguntas) {
            JSONObject formDetail = new JSONObject();
            formDetail.put("id", pregunta.getId());
            formDetail.put("pregunta", pregunta.getNombrePregunta());
            formDetail.put("tipo_pregunta",pregunta.getCategoria());

            List<Alternativa> alternativas = pregunta.getAlternativas();
            JSONArray example_a = new JSONArray();
            for (Alternativa alternativa : alternativas) {
                JSONObject example = new JSONObject();
                example.put("id", alternativa.getId());
                example.put("nombre", alternativa.getNombre());
                example_a.add(example);

            }
            formDetail.put("alternativas", example_a);
            jsonArray.add(formDetail);
        }
        //jsonArray.add(blank);
        JSONObject preguntasJsonObj = new JSONObject();
        responseDetail.put("response", jsonArray);
        responseDetail.put("status", true);

        return responseDetail.toString();
    }



}
