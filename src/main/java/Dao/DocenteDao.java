package Dao;

import Model.*;

import java.util.List;

public interface DocenteDao {
    void addDocente(Docente docente);
    void addAlternativa(Alternativa alternativa);
    Docente findByIdDocente(Integer integer);
    List<Resultado> obtenerResultados(Integer integer);

    void Test();


}
