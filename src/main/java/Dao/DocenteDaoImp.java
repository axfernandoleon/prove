package Dao;

import Model.*;

import Utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.sql.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("ALL")
public class DocenteDaoImp implements DocenteDao {
    private static final Logger logger = LoggerFactory.getLogger(DocenteDaoImp.class);
    private SessionFactory sessionFactory;


    @Override
    public void addDocente(Docente docente) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.save(docente);
        transaction.commit();
        System.out.println("save docente");
        session.close();
        /*   HibernateUtil.shutdown();*/

    }





    @Override
    public void addAlternativa(Alternativa alternativa) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.save(alternativa);
        transaction.commit();
        System.out.println("save alternativa");
        session.close();
        /*        HibernateUtil.shutdown();*/
    }


    @Override
    public Docente findByIdDocente(Integer id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Docente docente = (Docente) session.get(Docente.class, id);
        /*   List <Examen> examenCollection = docente.getExamenList();

        System.out.println("docente encontrado" +examenCollection.size());*/
        transaction.commit();
        session.close();
        return docente;
    }

    @Override
    public List<Resultado> obtenerResultados(Integer integer) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        String id_prueba = "FROM Resultado WHERE id_examen = "+ integer;
        Query query = session.createQuery(id_prueba);
        List<Resultado> results = query.list();
        transaction.commit();
        session.close();
        return results;
    }


    @Override
    public void Test() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        System.out.println("OK");
        session.close();

    }


}
