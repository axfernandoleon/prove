package Dao;

import Model.Examen;

public interface ExamenDao {
    void addExamen(Examen examen);
    Examen findByIdExamen(Integer integer);
    Examen findByNameExamen(String string);
    void obtenerExamenes(Integer id); //<Ex
    Examen getExamen(Integer id); //<Ex
    Examen checkStateExamen(Integer integer);




}
