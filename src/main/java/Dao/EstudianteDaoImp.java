package Dao;

import Model.Estudiante;
import Model.Examen;
import Utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EstudianteDaoImp implements  EstudianteDao {
    @Override
    public void addEstudiante(Estudiante estudiante) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.save(estudiante);
        transaction.commit();
        System.out.println("save estudiante");
        session.close();
        /*   HibernateUtil.shutdown();*/
    }
    @Override
    public Estudiante findByIdEstudiante(Integer integer) {
        return null;
    }

    @Override
    public Estudiante findByNameEstudiante(String string) {
        return null;
    }
}
