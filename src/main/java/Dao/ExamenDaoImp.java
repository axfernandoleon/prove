package Dao;

import Model.Examen;
import Utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ExamenDaoImp implements ExamenDao {
    @Override
    public void addExamen(Examen examen) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.save(examen);
        transaction.commit();
        System.out.println("save examen");
        session.close();
        /*  HibernateUtil.shutdown();*/
    }

    @Override
    public Examen findByIdExamen(Integer integer) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Examen examen = (Examen) session.get(Examen.class, integer);
        transaction.commit();
        session.close();
        return examen;
    }

    @Override
    public Examen findByNameExamen(String string) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        Transaction transaction = session.beginTransaction();
        Examen examen = (Examen) session.byNaturalId(Examen.class)
                .using("nombre", string).load();
        transaction.commit();
        session.close();
        return examen;
    }

    @Override
    public void obtenerExamenes(Integer id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Examen examen = session.get(Examen.class, id);
        System.out.println(examen.getNombre());
        transaction.commit();
        session.close();
    }

    @Override
    public Examen getExamen(Integer id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Examen examen = (Examen) session.get(Examen.class, id);
        transaction.commit();
        session.close();
        return examen;
    }

    @Override
    public Examen checkStateExamen(Integer integer) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Examen examen = (Examen) session.get(Examen.class, integer);
        transaction.commit();
        session.close();
        return examen;

    }


}
