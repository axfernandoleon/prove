package Dao;

import Model.Resultado;
import Utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ResultadoDaoImp implements ResultadoDao {
    @Override
    public void addResultado(Resultado resultado) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.save(resultado);
        transaction.commit();
        System.out.println("save resultado pregunta");
        session.close();
        /*  HibernateUtil.shutdown();*/
    }
}
