package Dao;

import Model.Pregunta;
import Utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PreguntaDaoImp implements PreguntaDao{
    @Override
    public void addPregunta(Pregunta pregunta) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.save(pregunta);
        transaction.commit();
        System.out.println("save pregunta");
        session.close();
        /*  HibernateUtil.shutdown();*/

    }

    @Override
    public Pregunta findByIdPregunta(Integer integer) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Pregunta pregunta = (Pregunta) session.get(Pregunta.class, integer);
        transaction.commit();
        session.close();
        return pregunta;
    }

    @Override
    public Pregunta findByNamePregunta(String string) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Pregunta pregunta = (Pregunta) session.byNaturalId(Pregunta.class)
                .using("nombre", string).load();
        transaction.commit();
        session.close();
        return pregunta;
    }


}
