package Dao;

import Model.Pregunta;

public interface PreguntaDao {
    void addPregunta(Pregunta pregunta);
    Pregunta findByIdPregunta(Integer integer);
    Pregunta findByNamePregunta(String string);

}
