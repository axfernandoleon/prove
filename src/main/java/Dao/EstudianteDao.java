package Dao;

import Model.Estudiante;

public interface EstudianteDao {
    void addEstudiante(Estudiante estudiante);
    Estudiante findByIdEstudiante(Integer integer);
    Estudiante findByNameEstudiante(String string);

}
