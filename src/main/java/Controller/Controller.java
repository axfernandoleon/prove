package Controller;

import Dao.*;
import Model.*;
import Utils.RestContext;
import WS.TestSocket;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.eclipse.jetty.websocket.api.Session;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static spark.Spark.*;


public class Controller {
    static String log4jConfPath = "/home/tony/repositorios/prove/src/main/resources/log4j.properties";
    public static Map<Session, String> userUsernameMap = new ConcurrentHashMap<>();
    public static int nextUserNumber = 1; //Used for creating the next username

    public static void main(String[] args) {
        webSocket("/socket", TestSocket.class);
        System.out.println();
        init();

/*Configuraciones de DOM*/
        RestContext.apply();

        /*Implementaciones de interfaces de DAO*/
        DocenteDaoImp docenteServiceImp = new DocenteDaoImp();
        ExamenDaoImp examenDaoImp = new ExamenDaoImp();
        PreguntaDaoImp preguntaDaoImp = new PreguntaDaoImp();
        EstudianteDaoImp estudianteDaoImp = new EstudianteDaoImp();
        ResultadoDaoImp resultadoDaoImp = new ResultadoDaoImp();


        /*Anadir un docente al sistema*/
        post("/docente/add", (request, response) -> {
            Docente objDocente = new Docente();
            String jsonArray = request.body();
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(jsonArray).getAsJsonObject();

            String docente = jsonObject.get("docente").toString().replace("\"", "");
            String password = jsonObject.get("password").toString().replace("\"", "");

            objDocente.setUsuario(docente);
            objDocente.setPass(password);
            docenteServiceImp.addDocente(objDocente);

            return new Gson().toJson(objDocente.toString());
        });

        /*anadir un nuevo examen*/
        post("/docente/examen", (request, response) -> {

            String json = String.valueOf(request.body());

            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();

            Integer id_docente = Integer.valueOf(jsonObject.get("id_docente").toString().replace("\"", ""));
            System.out.println(id_docente);
            Docente docente = docenteServiceImp.findByIdDocente(id_docente);
            System.out.println(docente.toString());

            Examen examen = new Examen();

            String nombre_examen = jsonObject.get("nombre_examen").toString().replace("\"", "");
            examen.setDocente(docente);
            examen.setNombre(nombre_examen);

            System.out.println(examen.getPreguntaList());
            examenDaoImp.addExamen(examen);
            return new Gson().toJson(examen.toString());
        });

        /*anadir una pregunta a  un examen*/

        post("/docente/examen/pregunta", (request, response) -> {
            /*Extraer datos*/


            String json = String.valueOf(request.body());
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();
            System.out.println(jsonObject);

            //*Obtener el examen*//*
            Integer id_examen = Integer.valueOf(jsonObject.get("id_examen").toString().replace("\"", ""));
            Examen examen = examenDaoImp.findByIdExamen(id_examen);
            System.out.println(examen.getNombre());
            Pregunta pregunta = new Pregunta();

            String nombre_pregunta = jsonObject.get("nombre_pregunta").toString().replace("\"", "");
            Integer tipo_pregunta = Integer.valueOf(jsonObject.get("tipo_pregunta").toString().replace("\"", ""));

            pregunta.setExamen(examen);
            pregunta.setNombrePregunta(nombre_pregunta);
            pregunta.setCategoria(tipo_pregunta);
            System.out.println(pregunta.getNombrePregunta());
            preguntaDaoImp.addPregunta(pregunta);
            List<String> listAlternativas = new ArrayList<String>();
            Pregunta preguntaID = preguntaDaoImp.findByNamePregunta(nombre_pregunta);
            String alter_uno;
            String alter_dos;
            String alter_tres;
            Alternativa alternativa = new Alternativa();
            alternativa.setPregunta(preguntaID);
            System.out.println("Soy la pregunta de tipo: "+ tipo_pregunta);

            /*Si el tipo de pregunta es multiopcion*/
            if(tipo_pregunta  == 1) {
                // Seleccion Multiple
                alter_uno = jsonObject.get("alter_uno").toString().replace("\"", "");
                alter_dos = jsonObject.get("alter_dos").toString().replace("\"", "");
                alter_tres = jsonObject.get("alter_tres").toString().replace("\"", "");
                listAlternativas.add(alter_uno);
                listAlternativas.add(alter_dos);
                listAlternativas.add(alter_tres);
                alternativa.setPregunta(preguntaID);
                for (String str : listAlternativas) {
                    alternativa.setNombre(str);
                    docenteServiceImp.addAlternativa(alternativa);
                }
                listAlternativas.clear();
                alternativa = null;
            }

            /*Si el tipo de pregunta en V o F*/
            if(tipo_pregunta == 3){
                    // True False
                    alter_uno = "Verdadero";
                    alter_dos = "Falso";
                    listAlternativas.add(alter_uno);
                    listAlternativas.add(alter_dos);
                    for (String str : listAlternativas) {
                        alternativa.setNombre(str);
                        docenteServiceImp.addAlternativa(alternativa);
                    }

                    listAlternativas.clear();
            }

            /*Si el tipo de pregunta es de asignar una respuesta corta*/

            if(tipo_pregunta == 4){
                System.out.println("Entre a 4");
                // True False
                alter_uno = jsonObject.get("alter_uno").toString().replace("\"", "");
                listAlternativas.add(alter_uno);
                for (String str : listAlternativas) {
                    alternativa.setNombre(str);
                    System.out.println("mIRAAAAA");
                    System.out.println(alternativa.getNombre());
                    docenteServiceImp.addAlternativa(alternativa);
                }
                listAlternativas.clear();
            }
            return new Gson().toJson(pregunta.toString());
    });


        // Obtiene las pruebas por el id del docente
        get("/docente/examenes/:id", (request, response) -> {
            response.type("application/json");
            JSONObject responseDetail = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            Integer id = Integer.valueOf(request.params("id"));
            Docente docente = docenteServiceImp.findByIdDocente(id);

            if (docente == null) {
                //JSONArray responseArrayError=  new JSONArray();
                JSONObject responseObjError = new JSONObject();
                responseObjError.put("code", response.status());
                responseObjError.put("status", false);
                responseObjError.put("response", "No se encontro el Elemento");
                return responseObjError;
            }

            List<Examen> examenList = docente.getExamenList();
            Set<Examen> linkhashSet = new HashSet<Examen>(examenList);
            linkhashSet.addAll(examenList);
            examenList.clear();
            examenList.clear();
            examenList.addAll(linkhashSet);
            for (Examen examen : examenList) {
                JSONObject formDetail = new JSONObject();
                formDetail.put("id", examen.getId());
                formDetail.put("nombre", examen.getNombre());
                jsonArray.add(formDetail);
            }
            responseDetail.put("response", jsonArray);
            responseDetail.put("status", true);
            responseDetail.put("code", response.status());

            return responseDetail;

        });

        // Obtiene el examen  con preguntas y alternativas
        get("/examen/:id", (request, response) -> {
                    response.type("application/json");
                    JSONObject responseDetail = new JSONObject();
                    JSONArray jsonArray = new JSONArray();

                        Integer id = Integer.valueOf(request.params("id"));
                    Examen examen = examenDaoImp.getExamen(id);

                    if (examen == null) {
                        JSONObject error = new JSONObject();
                        error.put("code", response.status());
                        error.put("status", false);
                        error.put("response", "No se encontro el examen");
                        return error;
                    }
                    List<Pregunta> preguntas = examen.getPreguntaList();

                    Set<Pregunta> linkhashSet = new HashSet<Pregunta>(preguntas);
                    linkhashSet.addAll(preguntas);
                    preguntas.clear();
                    preguntas.clear();
                    preguntas.addAll(linkhashSet);


                    System.out.println(preguntas.size());
                    for (Pregunta pregunta : preguntas) {
                        JSONObject formDetail = new JSONObject();
                        formDetail.put("id", pregunta.getId());
                        formDetail.put("pregunta", pregunta.getNombrePregunta());

                        List<Alternativa> alternativas = pregunta.getAlternativas();
                        JSONArray example_a = new JSONArray();
                        for (Alternativa alternativa : alternativas) {
                            JSONObject example = new JSONObject();
                            example.put("id", alternativa.getId());
                            example.put("nombre", alternativa.getNombre());
                            example_a.add(example);

                        }
                        formDetail.put("alternativas", example_a);
                        jsonArray.add(formDetail);
                    }
                    responseDetail.put("response", jsonArray);
                    responseDetail.put("status", true);
                    responseDetail.put("code", response.status());

                    return responseDetail;
                }
        );


        /*Busca un examen por el nombre del examen*/

        get("/examen/buscar/:nombre", (request, response) -> {
                    response.type("application/json");
                    JSONArray jsonArray = new JSONArray();
                    String str = String.valueOf(request.params("nombre"));
                    Examen examen = examenDaoImp.findByNameExamen(str);
                    if (examen == null) {
                        JSONObject error = new JSONObject();
                        error.put("code", response.status());
                        error.put("status", false);
                        error.put("response", "No se encontro el examen");
                        return error;
                    }
                    JSONObject responseObj = new JSONObject();
                    JSONObject id = new JSONObject();
                    id.put("id", examen.getId());
                    responseObj.put("response", id);
                    responseObj.put("code", response.status());
                    responseObj.put("status", true);
                    jsonArray.add(responseObj);
                    return responseObj;
                }
        );

        /*busca una pregutna por el nombre*/
        get("/pregunta/buscar/:nombre", (request, response) -> {
                    response.type("application/json");
                    JSONObject responseDetail = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    String str = String.valueOf(request.params("nombre"));
                    Pregunta pregunta = preguntaDaoImp.findByNamePregunta(str);
                    if (pregunta == null) {
                        JSONObject error = new JSONObject();
                        error.put("code", response.status());
                        error.put("status", false);
                        error.put("response", "No se encontro el examen");
                        return error;
                    }

                    JSONObject responseObj = new JSONObject();
                    JSONObject id = new JSONObject();
                    id.put("id", pregunta.getId());
                    responseObj.put("response", id);
                    responseObj.put("code", response.status());
                    responseObj.put("status", true);
                    jsonArray.add(responseObj);
                    return responseObj;
                }
        );

        /*simple logeo por id del docente*/

        get("/logeo/:id", (request, response) -> {
                    response.type("application/json");
                    JSONArray jsonArray = new JSONArray();
                    Integer id = Integer.valueOf(request.params("id"));
                    Docente docente = docenteServiceImp.findByIdDocente(id);
                    if (docente == null) {
                        JSONObject error = new JSONObject();
                        error.put("code", response.status());
                        error.put("status", false);
                        error.put("response", "No se encontro el docente");
                        return error;
                    }

                    JSONObject responseJsonObj = new JSONObject();
                    JSONObject docenteJsonObj = new JSONObject();
                    docenteJsonObj.put("id", docente.getId());
                    docenteJsonObj.put("usuario", docente.getUsuario());
                    responseJsonObj.put("response", docenteJsonObj);
                    responseJsonObj.put("code", response.status());
                    responseJsonObj.put("status", true);
                    jsonArray.add(responseJsonObj);
                    return responseJsonObj;
                }
        );

        /*Obtener el estado del examen */
        get("/examen/status/:id", (request, response) -> {
                    response.type("application/json");
                    JSONArray jsonArray = new JSONArray();
                    Integer id = Integer.valueOf(request.params("id"));
                    Examen examen = examenDaoImp.checkStateExamen(id);

                    /*En caso que el examen sea null*/
                    if (Objects.isNull(examen)) {
                        JSONObject error = new JSONObject();
                        error.put("code", response.status());
                        error.put("status", false);
                        error.put("response", "No existe la prueba");
                        return error;
                    }
                    /*System.out.println(examen.getActivo());
                    */

                    Integer state = examen.getActivo();
                    /*Si el examen esta inactivo*/
                    if (state == 0) {
                        JSONObject error = new JSONObject();
                        error.put("code", response.status());
                        error.put("status", false);
                        error.put("response", "La prueba no se encuentra activa");
                        return error;
                    }

                    /*En caso que el examen este activado*/

                    JSONObject responseJsonObj = new JSONObject();
                    JSONObject docenteJsonObj = new JSONObject();
                    docenteJsonObj.put("id", examen.getId());
                    responseJsonObj.put("response", docenteJsonObj);
                    responseJsonObj.put("code", response.status());
                    responseJsonObj.put("status", true);
                    jsonArray.add(responseJsonObj);
                    return responseJsonObj;

                }
        );


        /*Se guarda el estudiante que ingreso a la prueba*/
        post("/estudiante/add", (request, response) -> {
            response.type("application/json");

            Estudiante objEstudiante = new Estudiante();
            String jsonArray = request.body();
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(jsonArray).getAsJsonObject();

            String nombre = jsonObject.get("nombre_estudiante").toString().replace("\"", "");

            objEstudiante.setNombre(nombre);

            estudianteDaoImp.addEstudiante(objEstudiante);

            return new Gson().toJson(objEstudiante.toString());
        });

        /*Se guarda la respuesta del estudiante*/

        post("/estudiante/respuesta/save", (request, response) -> {
            response.type("application/json");
            Resultado objResultado = new Resultado();
            String jsonArray = request.body();
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(jsonArray).getAsJsonObject();
            System.out.println(jsonObject);

            /*Se mapea las respuestas*/
            String nombre_estudiante = jsonObject.get("nombre_estudiante").toString().replace("\"", "");
            String respuestaSelect = jsonObject.get("respuestaSelect").toString().replace("\"", "");
            Integer id_examen = Integer.valueOf(jsonObject.get("id_examen").toString().replace("\"", ""));
            Integer id_pregunta = Integer.valueOf(jsonObject.get("id_pregunta").toString().replace("\"", ""));
            System.out.println(nombre_estudiante +" -- " +respuestaSelect + " -- " + id_examen + " -- "+ id_pregunta);
            objResultado.setId_examen(id_examen);
            objResultado.setId_pregunta(id_pregunta);
            objResultado.setNombre_estudiante(nombre_estudiante);
            objResultado.setRespuestaSelect(respuestaSelect);
            System.out.println(objResultado.getRespuestaSelect());


            resultadoDaoImp.addResultado(objResultado);

            return new Gson().toJson(objResultado.getRespuestaSelect() + "--"+objResultado.getId_pregunta());

        });

        /*Descarga los resultados por el ID*/
        get("/examen/resultados/:id", (request, response) -> {
            response.header("Content-disposition", "attachment; filename=resultados.csv;");
            Integer id = Integer.valueOf(request.params("id"));
            FileWriter writer = new FileWriter("resultados.csv");
            List<Resultado> resultadoList = docenteServiceImp.obtenerResultados(id);
            try{
                writer.append("Estudiante,Pregunta,Respuesta\n");
                for (Resultado resultado :resultadoList){

                    writer.append(resultado.getNombre_estudiante());
                    writer.append(",");
                    writer.append(resultado.getId_pregunta().toString());
                    writer.append(",");
                    writer.append(resultado.getRespuestaSelect());
                    writer.append("\n");
                }

            }catch (Exception e){
                System.out.println("Error in Csv");
                e.printStackTrace();
            }finally {
                try{
                    writer.flush();
                    writer.close();
                    System.out.println("OK");
                }catch (IOException e){
                    System.out.println("Error en el flusing");
                    e.printStackTrace();
                }
            }

            File file = new File("resultados.csv");
            OutputStream outputStream = response.raw().getOutputStream();
            outputStream.write(Files.readAllBytes(file.toPath()));
            outputStream.flush();
          return response;
        }
        );
    }



}


