-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-01-2019 a las 05:33:06
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `provedb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Alternativa`
--

CREATE TABLE `Alternativa` (
  `idalternativa` int(11) NOT NULL,
  `nombreAlternativa` text COLLATE utf8_bin NOT NULL,
  `Pregunta_idPregunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Categoria`
--

CREATE TABLE `Categoria` (
  `idcategoria` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `Categoria`
--

INSERT INTO `Categoria` (`idcategoria`, `nombre`) VALUES
(1, 'Multi-Opcion'),
(3, 'Verdadero o Falso'),
(4, 'Respuesta Corta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Docente`
--

CREATE TABLE `Docente` (
  `idDocente` int(11) NOT NULL,
  `usuario` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `pass` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `Docente`
--

INSERT INTO `Docente` (`idDocente`, `usuario`, `pass`) VALUES
(1, 'Juan001', '123456'),
(2, 'axfrlm', 'alex123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Estudiante`
--

CREATE TABLE `Estudiante` (
  `idEstudiante` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Examen`
--

CREATE TABLE `Examen` (
  `idExamen` int(11) NOT NULL,
  `nombreExamen` varchar(45) COLLATE utf8_bin NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `Docente_idDocente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `Examen`
--

INSERT INTO `Examen` (`idExamen`, `nombreExamen`, `activo`, `Docente_idDocente`) VALUES
(63, 'Examen Matematicas', 1, 1),
(64, 'Examen de Ciencias Naturales', 1, 1),
(69, 'examen123', 1, 1),
(70, 'Test Examen', 1, 1),
(71, 'Examen1001', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(72),
(72),
(72),
(72);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pregunta`
--

CREATE TABLE `Pregunta` (
  `idPregunta` int(11) NOT NULL,
  `nombrePregunta` text COLLATE utf8_bin NOT NULL,
  `Examen_idExamen` int(11) NOT NULL,
  `Categoria_idCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `Pregunta`
--

INSERT INTO `Pregunta` (`idPregunta`, `nombrePregunta`, `Examen_idExamen`, `Categoria_idCategoria`) VALUES
(1, 'Cuanto es 4+4?', 63, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Respuesta`
--

CREATE TABLE `Respuesta` (
  `idRespuesta` int(11) NOT NULL,
  `nombreRespuesta` text COLLATE utf8_bin NOT NULL,
  `Pregunta_idPregunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Resultado`
--

CREATE TABLE `Resultado` (
  `idResultado` int(11) NOT NULL,
  `Pregunta_idPregunta` int(11) NOT NULL,
  `Estudiante_idEstudiante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Alternativa`
--
ALTER TABLE `Alternativa`
  ADD PRIMARY KEY (`idalternativa`),
  ADD KEY `FKigt9h2s7r9r0sc5be4byo229v` (`Pregunta_idPregunta`);

--
-- Indices de la tabla `Categoria`
--
ALTER TABLE `Categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `Docente`
--
ALTER TABLE `Docente`
  ADD PRIMARY KEY (`idDocente`),
  ADD UNIQUE KEY `nombre` (`usuario`);

--
-- Indices de la tabla `Estudiante`
--
ALTER TABLE `Estudiante`
  ADD PRIMARY KEY (`idEstudiante`);

--
-- Indices de la tabla `Examen`
--
ALTER TABLE `Examen`
  ADD PRIMARY KEY (`idExamen`),
  ADD UNIQUE KEY `UKmuh73lq7u4e56kdanar3ypm3s` (`nombreExamen`),
  ADD KEY `FKi5l5m72hxtxciv3wicnjfu1nb` (`Docente_idDocente`);

--
-- Indices de la tabla `Pregunta`
--
ALTER TABLE `Pregunta`
  ADD PRIMARY KEY (`idPregunta`),
  ADD KEY `FKr3ko4l0t7pueqy0nljtb6wlhn` (`Examen_idExamen`),
  ADD KEY `Categoria_idCategoria` (`Categoria_idCategoria`);

--
-- Indices de la tabla `Respuesta`
--
ALTER TABLE `Respuesta`
  ADD PRIMARY KEY (`idRespuesta`),
  ADD KEY `Pregunta_idPregunta` (`Pregunta_idPregunta`);

--
-- Indices de la tabla `Resultado`
--
ALTER TABLE `Resultado`
  ADD PRIMARY KEY (`idResultado`),
  ADD KEY `Pregunta_idPregunta_Resul` (`Pregunta_idPregunta`),
  ADD KEY `Estudiante_idEstudiante_Resul` (`Estudiante_idEstudiante`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Alternativa`
--
ALTER TABLE `Alternativa`
  MODIFY `idalternativa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Categoria`
--
ALTER TABLE `Categoria`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `Docente`
--
ALTER TABLE `Docente`
  MODIFY `idDocente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `Examen`
--
ALTER TABLE `Examen`
  MODIFY `idExamen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT de la tabla `Pregunta`
--
ALTER TABLE `Pregunta`
  MODIFY `idPregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `Respuesta`
--
ALTER TABLE `Respuesta`
  MODIFY `idRespuesta` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Alternativa`
--
ALTER TABLE `Alternativa`
  ADD CONSTRAINT `FKigt9h2s7r9r0sc5be4byo229v` FOREIGN KEY (`Pregunta_idPregunta`) REFERENCES `Pregunta` (`idPregunta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Examen`
--
ALTER TABLE `Examen`
  ADD CONSTRAINT `FKi5l5m72hxtxciv3wicnjfu1nb` FOREIGN KEY (`Docente_idDocente`) REFERENCES `Docente` (`idDocente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Pregunta`
--
ALTER TABLE `Pregunta`
  ADD CONSTRAINT `Categoria_idCategoria` FOREIGN KEY (`Categoria_idCategoria`) REFERENCES `Categoria` (`idcategoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKr3ko4l0t7pueqy0nljtb6wlhn` FOREIGN KEY (`Examen_idExamen`) REFERENCES `Examen` (`idExamen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Respuesta`
--
ALTER TABLE `Respuesta`
  ADD CONSTRAINT `Pregunta_idPregunta` FOREIGN KEY (`Pregunta_idPregunta`) REFERENCES `Pregunta` (`idPregunta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Resultado`
--
ALTER TABLE `Resultado`
  ADD CONSTRAINT `Estudiante_idEstudiante_Resul` FOREIGN KEY (`Estudiante_idEstudiante`) REFERENCES `Estudiante` (`idEstudiante`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Pregunta_idPregunta_Resul` FOREIGN KEY (`Pregunta_idPregunta`) REFERENCES `Pregunta` (`idPregunta`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
