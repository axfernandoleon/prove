# Prove
Evaluación en linea para fines Educativos

1. [Resumen](https://gitlab.com/axfernandoleon/prove/wikis/resumen?)

2. [Patrones Ocupados](https://gitlab.com/axfernandoleon/prove/wikis/patterns)

3. [Librerias Ocupadas](https://gitlab.com/axfernandoleon/prove/wikis/librerias)

4. [Base de Datos](https://gitlab.com/axfernandoleon/prove/wikis/bd)

5. [Diagrama](https://gitlab.com/axfernandoleon/prove/blob/master/imgs/PreguntaDaoImp.png) 




