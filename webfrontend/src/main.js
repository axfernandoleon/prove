import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './store'
import Vuetify from 'vuetify'
import 'bootstrap-material-design/dist/css/bootstrap-material-design.min.css';

/**
import  'jquery/dist/jquery.js'
import jq from 'jquery/dist/jquery.js'
import 'popper.js/dist/popper.js'
window.$=jq
window.JQuery=jq
import 'bootstrap-material-design/dist/js/bootstrap-material-design.js' */
Vue.config.productionTip = false
Vue.use(Vuetify)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
