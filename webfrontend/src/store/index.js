import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        id_docente:null,
        id_examen:'',
        id_estudiante:'',
    },
    mutations:{
        changed_docente(state,id_docente){
            state.id_docente = id_docente;
        },
        changed_examen(state,id_examen){
            state.id_examen = id_examen;
        }
    },
    getters:{
        id_docente : state => state.id_docente,
        id_examen : state => state.id_examen
    }
});