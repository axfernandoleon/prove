import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

import Goodbye from '../components/Goodbye.vue'
import MostrarExamen from '../components/MostrarExamen.vue'
import Login from '../components/Login.vue';
import List from '../components/List.vue';
import Hello from '../components/Hello.vue'
import CrearExamen from '../components/CrearExamen.vue'
import EstudianteExamen from '../components/EstudianteExamen.vue'

// import CrearExamen from '../components/CrearExamen.vue'
export default new Router({
    mode: 'history',
    routes: [
    {
      path: '/',
      redirect:{
          name:'hello'
      },
      
    },
    {
        path:'/goodbye',
        name:'goodbye',
        component:Goodbye
    },
    {
        path:'/hello',
        name:'hello',
        component:Hello
    },
    {
        path:'/examen',
        name:'examen',
        component:MostrarExamen
    },
    {
        path:'/addExamen',
        name:'addExamen',
        component:CrearExamen
    },
    {
        path:'/login',
        name:'login',
        component:Login
    },
    {
        path:'/list',
        name:'list',
        component:List
    },
    {
        path:'/liveExamen',
        name:'liveExamen',
        component:EstudianteExamen
    }
  ]
})